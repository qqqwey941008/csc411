import numpy as np
from l2_distance import l2_distance
import matplotlib.pyplot as plt
import scipy.io as sio
from sklearn import cross_validation

def run_knn(k, train_data, train_labels, valid_data):
    """Uses the supplied training inputs and labels to make
    predictions for validation data using the K-nearest neighbours
    algorithm.

    Note: N_TRAIN is the number of training examples,
          N_VALID is the number of validation examples, 
          and M is the number of features per example.

    Inputs:
        k:            The number of neighbours to use for classification 
                      of a validation example.
        train_data:   The N_TRAIN x M array of training
                      data.
        train_labels: The N_TRAIN x 1 vector of training labels
                      corresponding to the examples in train_data 
                      (must be binary).
        valid_data:   The N_VALID x M array of data to
                      predict classes for.

    Outputs:
        valid_labels: The N_VALID x 1 vector of predicted labels 
                      for the validation data.
    """
    
    dist = l2_distance(valid_data.T, train_data.T)
    nearest = np.argsort(dist, axis=1)[:,:k]
    
    train_labels = train_labels.reshape(-1)
    valid_labels = train_labels[nearest]
    print(valid_labels)

    # note this only works for binary labels
    valid_labels = (np.mean(valid_labels, axis=1) >= 0.5).astype(np.int)
    valid_labels = valid_labels.reshape(-1,1)

    return valid_labels

if __name__ == '__main__':
    #read data
    data = sio.loadmat('labeled_images.mat')
    image = np.transpose(data['tr_images'])
    x,y,z = image.shape
    image = image.reshape(x,y*z)
    labels = data['tr_labels']
    #shuffle
    train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
        image, labels, test_size=0.3, random_state=1)
    for i in range(1,10,2):
        validation_p = run_knn(i, train_data,
                               train_labels,
                               valid_data)
        count = 0
        print(validation_p)
        print('-----------------------------------------')
        print(valid_labels)
        for j in range(len(validation_p)):
            if validation_p[j][0] == valid_labels[j][0]:
                count += 1
        print('K=' + str(i) +', Classification rate=' +
              str(count * 100.0/
                  len(validation_p)) +'%')
    '''
    for f in range(1,10):
        train_data = np.hstack((image[:(f-1)*l], image[f*l:])) 
        valid_data = image[(f-1)*l:f*l]
        print(train_data.shape)
        print('----------------------------------')
        print(valid_data.shape)
        train_labels = np.hstack(labels[:(f-1)*l], labels[f*l:])
        valid_labels = labels[(f-1)*l:f*l]
    
    for key in data:
        print(key)
        print(data[key])
        print('-------------------------------------------------')
    data = np.load('digits.npz')
    inputs_valid = np.transpose(np.hstack((data['valid2'], data['valid3'])))
    inputs_test = np.transpose(np.hstack((data['test2'], data['test3'])))
    target_train = np.transpose(np.hstack((np.zeros((1, data['train2'].shape[1])), np.ones((1, data['train3'].shape[1])))))
    target_valid = np.transpose(np.hstack((np.zeros((1, data['valid2'].shape[1])), np.ones((1, data['valid3'].shape[1])))))
    target_test = np.transpose(np.hstack((np.zeros((1, data['test2'].shape[1])), np.ones((1, data['test3'].shape[1])))))
    graph = []
    
    for i in range(1,10,2):
        validation_p = run_knn(i, inputs_train,
                    target_train,
                    inputs_valid)
        count = 0
        for j in range(len(validation_p)):
            if validation_p[j][0] == target_valid[j][0]:
                count += 1
        graph.append(count * 100.0/len(validation_p))
        print('K=' + str(i) +', Classification rate=' +
              str(count * 100.0/
                  len(validation_p)) +'%')
    plt.plot([1,3,5,7,9], graph)
    plt.axis([0, 10, 80, 100])
    plt.show()
    graph = []
    for i in range(1,10,2):
        validation_p = run_knn(i, inputs_train,
                    target_train,
                    inputs_test)
        count = 0
        for j in range(len(validation_p)):
            if validation_p[j][0] == target_test[j][0]:
                count += 1
        print('K=' + str(i) +', Classification rate=' +
              str(count * 100.0/len(validation_p)) +'%')  
        graph.append(count * 100.0/len(validation_p))
    plt.plot([1,3,5,7,9], graph)
    plt.show()
    '''