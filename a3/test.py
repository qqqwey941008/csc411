import numpy as np

import scipy.io as sio
import matplotlib.pyplot as plt
from sklearn import mixture
from sklearn import cross_validation, linear_model, datasets, metrics

def get_class_rate(predictions, targets):
	count = 0
	num_points = len(predictions)
	
	for i in range(num_points):
		if predictions[i] == targets[i]:
			count += 1
	
	return float(count) / float(num_points)

def run_mog(train_images, train_labels, valid_images, valid_labels):
	class_rate = []
	k = []
	for i in range(1, 50):
		k.append(i)
		# Run KNN
		classifier = mixture.GMM(n_components=i)
		classifier.fit(train_images, train_labels)
		preds = classifier.predict(valid_images)
	
		# Get class rate
		class_rate.append(get_class_rate(preds, valid_labels))
		# show_test_results(valid_images[:100], valid_labels[:100], preds[:100])
	plt.plot(k, class_rate)
	plt.ylabel('Classification rate')
	plt.xlabel('k')
	plt.show()
if __name__ == '__main__':
	data = sio.loadmat('labeled_images.mat')
	image = np.transpose(data['tr_images'])
	identity = data['tr_identity'].reshape(len(data['tr_labels']),) 
	x,y,z = image.shape
	image = image.reshape(x,y*z)
	labels = data['tr_labels'].reshape(len(data['tr_labels']),)  
	
	id_list = identity.tolist()
	i=0
	for idf in id_list:
		if idf != -1 and id_list.count(idf) == 1:
			identity[i] = SINGLETON_ID
			i += 1
			
	train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
	        image, labels, test_size=0.25, random_state=1, stratify=identity)  			
	for i in range(7, 50, 7):
		# Run KNN
		classifier = mixture.GMM(n_components=i)
		classifier.fit(train_data, train_labels)
		preds = classifier.predict(valid_data)
		count = 0
		for j in range(len(preds)):
			if preds[j] == valid_labels[j]:
				count += 1
		print(preds)
		print('K=' + str(i) +', Classification rate=' + str(count * 100.0/len(preds)))		