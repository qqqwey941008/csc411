import numpy as np
from sklearn.decomposition import PCA,KernelPCA
from sklearn.svm import SVC
import scipy.io as sio
from sklearn.metrics import classification_report as report
from sklearn import cross_validation
import pandas as pd
from util import *
from sklearn.metrics import accuracy_score as score
from sklearn import preprocessing

def svm():
    identity, image, labels=load_train()
    #split
    train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(\
                image, labels, test_size=0.25, random_state=24,stratify=identity) 
    train_data, valid_data=preprocessing.normalize(train_data, norm='l2'),preprocessing.normalize(valid_data, norm='l2')
    COMPONENT_NUM = 20
    #do dimension reduction via pca
    #pca=PCA()
    #pca = PCA(n_components=COMPONENT_NUM, whiten=True)
    #pca = KernelPCA(kernel="linear", fit_inverse_transform=True,gamma=10)
    #pca.fit(train_data)
    #train_data = pca.transform(train_data)
    
    print('Train SVM...')
    #kernel choice
    #'linear', 'poly', 'rbf', 'sigmoid', 'precomputed'
    svc = SVC(kernel='linear',C=100.0,gamma=0.0001)
    svc.fit(train_data, train_labels)
    #test= sio.loadmat('public_test_images.mat')
    
    print('Predicting...')
    valid_data = np.array(valid_data)
    #valid_data = pca.transform(valid_data)
    predict = svc.predict(valid_data)
    #myfile = open(..., 'wb')
    #wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    #wr.writerow(mylist) 
    print len(predict)
    print score(predict,valid_labels)
    #print report(predict, valid_labels)
    
    print 'predict on the test set'
    test_input=load_public_test()
    #identity=[i for i in range(419)]
    predict_test=svc.predict(test_input)
    my_df = pd.DataFrame(predict_test)
    my_df.to_csv('result.csv', index=False, header=False)  

    
if __name__ == '__main__':
    svm()
