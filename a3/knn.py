import numpy as np
import scipy.io as sio
from sklearn import cross_validation
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report as report

if __name__ == '__main__':
    #read data
    data = sio.loadmat('labeled_images.mat')
    image = np.transpose(data['tr_images'])
    
    x,y,z = image.shape
    image = image.reshape(x,y*z)
    labels = data['tr_labels'].reshape(len(data['tr_labels']),)
    #image /= image.std()
    
    #shuffle
    train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
        image, labels, test_size=0.25, random_state=123)
    #knn
    plot = []
    graph = []
    for i in range(1,50,7):
        plot.append(i)
        neigh = KNeighborsClassifier(n_neighbors=i,weights='distance', algorithm='auto')
        neigh.fit(train_data, train_labels)
        
        count = 0
        validation_p = neigh.predict(valid_data)
        expression_rate = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
        for j in range(len(validation_p)):
            expression_rate[valid_labels[j]-1][1] += 1
            if validation_p[j] == valid_labels[j]:
                count += 1
                expression_rate[valid_labels[j]-1][0] += 1
        graph.append(count * 100.0/len(validation_p))
        if(i==8):
            print(report(validation_p, valid_labels))
        print('-----------------------------------------------------------')
        print('K=' + str(i) +', Classification rate=' + str(count * 100.0/len(validation_p)))
        for j in range(len(expression_rate)):
            print(str(j+1) + ', rate:' + str(expression_rate[j][0]* 100.0/expression_rate[j][1] ))
        for j in range(len(expression_rate)):
            print(str(expression_rate[j][0]* 100.0/expression_rate[j][1] ))
        print('-----------------------------------------------------------')

    plt.xlabel('K value')
    plt.ylabel('classification rate')
    plt.title('plot for classification rate against k value')    
    
    plt.plot(plot, graph)
    plt.axis([0, 50, 40, 60])
    plt.show()