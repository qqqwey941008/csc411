import numpy as np
import scipy.io as sio
from sklearn import cross_validation
import pandas as pd
#merge identity that has length less than 2
def transform(identity):
    identity_list = identity.tolist()
    collection=[]
    for idn in range(len(identity_list)):
        collection.append(identity_list[idn][0])
    for i in range(len((collection))):
        if collection.count(collection[i]) < 2:
            collection[i] = 0
    return np.array(collection)

def load_train():
    data = sio.loadmat('labeled_images.mat')
    image = np.transpose(data['tr_images'])
    x,y,z = image.shape
    image = image.reshape(x,y*z)    
    labels = data['tr_labels'].reshape(len(data['tr_labels']),)
    identity=data['tr_identity']
    identity=transform(identity)
    return identity, image, labels
def load_public_test():
    test_data = sio.loadmat('public_test_images.mat')
    #public_test_images
    test_image = np.transpose(test_data['public_test_images'])
    d1,d2,d3 = test_image.shape
    test_input = test_image.reshape(d1,d2*d3)    
    return test_input

#--------test--------------
#if __name__ == '__main__':
   # identity, image, labels=load_train()
    #train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
              #  image, labels, test_size=0.20, random_state=123,stratify=identity) 

    #my_df = pd.DataFrame(iden)
    #my_df.to_csv('result.csv', index=False, header=False)        