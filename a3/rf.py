import numpy as np
import scipy.io as sio
from sklearn import cross_validation, linear_model, datasets, metrics
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report as report

if __name__ == '__main__':
    #read data
    data = sio.loadmat('labeled_images.mat')
    image = np.transpose(data['tr_images'])
    x,y,z = image.shape
    image = image.reshape(x,y*z)
    labels = data['tr_labels'].reshape(len(data['tr_labels']),)
    #shuffle
    
    plot = []
    graph = []
    for i in range(100, 1000, 200):
        plot.append(i)
        train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
            image, labels, test_size=0.25, random_state=i)        
        rf = RandomForestClassifier(n_estimators=i, max_features='sqrt')
        rf.fit(train_data, train_labels)
        pred = rf.predict(valid_data)
        count = 0
        expression_rate = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
        for j in range(len(pred)):
            expression_rate[valid_labels[j]-1][1] += 1
            if pred[j] == valid_labels[j]:
                count += 1
                expression_rate[valid_labels[j]-1][0] += 1
        graph.append(count * 100.0/len(pred))
        if(i==100):
            print(report(pred, valid_labels))
        print('-----------------------------------------------------------')
        print('n_estimators=' + str(i) + ' Classification rate=' + str(count * 100.0/len(pred)))
        for j in range(len(expression_rate)):
            print(str(j+1) + ', rate:' + str(expression_rate[j][0]* 100.0/expression_rate[j][1] ))
        for j in range(len(expression_rate)):
            print(str(expression_rate[j][0]* 100.0/expression_rate[j][1] ))
        print('-----------------------------------------------------------')

    plt.xlabel('n_estimators')
    plt.ylabel('classification rate')
    plt.title('plot for classification rate against n_estimators')    
    
    plt.plot(plot, graph)
    plt.axis([0, 1000, 50, 70])
    plt.show()        
    '''
    train_data = image
    train_labels = labels
    valid = sio.loadmat('public_test_images.mat')
    valid_data = np.transpose(valid['public_test_images'])
    x,y,z = valid_data.shape
    valid_data = valid_data.reshape(x,y*z)
    #valid_labels = valid['tr_labels'].reshape(len(valid['tr_labels']),)
    rf = RandomForestClassifier(n_estimators= 600, max_features='sqrt')
    rf.fit(train_data, train_labels)   
    pred = rf.predict(valid_data)
    my_df = pd.DataFrame(pred)
    my_df.to_csv('sub.csv', index=True, header=True)
    '''
    