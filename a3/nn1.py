'''
from lasagne.layers import DenseLayer
from lasagne.layers import InputLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import Conv2DLayer
from lasagne.layers import MaxPool2DLayer
from lasagne.nonlinearities import softmax
from lasagne.updates import adam
from lasagne.layers import get_all_params
from nolearn.lasagne import NeuralNet
from nolearn.lasagne import TrainSplit
from nolearn.lasagne import objective
'''


import lasagne
from lasagne import layers
from lasagne.updates import nesterov_momentum
from nolearn.lasagne import NeuralNet
from nolearn.lasagne import visualize

import numpy as np
import scipy.io as sio
from sklearn import cross_validation, linear_model, datasets, metrics

def regularization_objective(layers, lambda1=0., lambda2=0., *args, **kwargs):
    # default loss
    losses = objective(layers, *args, **kwargs)
    # get the layers' weights, but only those that should be regularized
    # (i.e. not the biases)
    weights = get_all_params(layers[-1], regularizable=True)
    # sum of absolute weights for L1
    sum_abs_weights = sum([abs(w).sum() for w in weights])
    # sum of squared weights for L2
    sum_squared_weights = sum([(w ** 2).sum() for w in weights])
    # add weights to regular loss
    losses += lambda1 * sum_abs_weights + lambda2 * sum_squared_weights
    return losses

if __name__ == '__main__':
    #read data
    data = sio.loadmat('labeled_images.mat')
    image = np.transpose(data['tr_images'])
    #x,y,z = image.shape
    #image = image.reshape(x,1,y,z)
    
    labels = data['tr_labels'].reshape(len(data['tr_labels']),)
    labels = labels - 1
    #shuffle
    train_data, valid_data, train_labels, valid_labels = cross_validation.train_test_split(
        image, labels, test_size=0.25, random_state=1)
    
    net1 = NeuralNet(
            layers=[('input', layers.InputLayer),
                    ('hidden', layers.DenseLayer),
                    ('output', layers.DenseLayer),
                    ],
            # layer parameters:
            input_shape=(None, 32, 32),
            hidden_num_units=1000, # number of units in 'hidden' layer
            output_nonlinearity=None,
            output_num_units=7, 
    
            # optimization method:
            update=lasagne.updates.adam,
            update_learning_rate=0.01,
    
            max_epochs=10,
            verbose=1,
            )

    
    
    
    '''
    layers0 = [
        # layer dealing with the input data
        (InputLayer, {'shape': (None, 1, 32, 32)}),
    
        # first stage of our convolutional layers
        (Conv2DLayer, {'num_filters': 96, 'filter_size': 5}),
        (Conv2DLayer, {'num_filters': 96, 'filter_size': 3}),
        (Conv2DLayer, {'num_filters': 96, 'filter_size': 3}),
        (Conv2DLayer, {'num_filters': 96, 'filter_size': 3}),
        (Conv2DLayer, {'num_filters': 96, 'filter_size': 3}),
        (MaxPool2DLayer, {'pool_size': 2}),
    
        # second stage of our convolutional layers
        (Conv2DLayer, {'num_filters': 128, 'filter_size': 3}),
        (Conv2DLayer, {'num_filters': 128, 'filter_size': 3}),
        (Conv2DLayer, {'num_filters': 128, 'filter_size': 3}),
        (MaxPool2DLayer, {'pool_size': 2}),
    
        # two dense layers with dropout
        (DenseLayer, {'num_units': 64}),
        (DropoutLayer, {}),
        (DenseLayer, {'num_units': 64}),
    
        # the output layer
        (DenseLayer, {'num_units': 7, 'nonlinearity': softmax}),
    ]    
    
    net0 = NeuralNet(
        layers=layers0,
        max_epochs=10,
    
        update=adam,
        update_learning_rate=0.0002,
    
        objective=regularization_objective,
        objective_lambda2=0.0025,
    
        train_split=TrainSplit(eval_size=0.25),
        verbose=1,
    )   
    '''
    net1.fit(train_data, train_labels)
    pred = net1.predict(valid_data)
    
    
    count = 0
    for j in range(len(pred)):
        if pred[j] == valid_labels[j]:
            count += 1
    print('Classification rate=' + str(count * 100.0/len(pred)))        