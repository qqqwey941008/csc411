import numpy as np
from l2_distance import l2_distance
import matplotlib.pyplot as plt

def run_knn(k, train_data, train_labels, valid_data):
    """Uses the supplied training inputs and labels to make
    predictions for validation data using the K-nearest neighbours
    algorithm.

    Note: N_TRAIN is the number of training examples,
          N_VALID is the number of validation examples, 
          and M is the number of features per example.

    Inputs:
        k:            The number of neighbours to use for classification 
                      of a validation example.
        train_data:   The N_TRAIN x M array of training
                      data.
        train_labels: The N_TRAIN x 1 vector of training labels
                      corresponding to the examples in train_data 
                      (must be binary).
        valid_data:   The N_VALID x M array of data to
                      predict classes for.

    Outputs:
        valid_labels: The N_VALID x 1 vector of predicted labels 
                      for the validation data.
    """

    dist = l2_distance(valid_data.T, train_data.T)
    nearest = np.argsort(dist, axis=1)[:,:k]

    train_labels = train_labels.reshape(-1)
    valid_labels = train_labels[nearest]

    # note this only works for binary labels
    valid_labels = (np.mean(valid_labels, axis=1) >= 0.5).astype(np.int)
    valid_labels = valid_labels.reshape(-1,1)

    return valid_labels

if __name__ == '__main__':
    train = np.load('mnist_train.npz')
    valid = np.load('mnist_valid.npz')
    test = np.load('mnist_test.npz')
    graph = []
    
    for i in range(1,10,2):
        validation_p = run_knn(i, train['train_inputs'],
                    train['train_targets'],
                    valid['valid_inputs'])
        count = 0
        for j in range(len(validation_p)):
            if validation_p[j][0] == valid['valid_targets'][j][0]:
                count += 1
        graph.append(count * 100.0/len(validation_p))
        print('K=' + str(i) +', Classification rate=' +
              str(count * 100.0/
                  len(validation_p)) +'%')
    plt.plot([1,3,5,7,9], graph)
    plt.axis([0, 10, 80, 100])
    plt.show()
    graph = []
    for i in range(1,10,2):
        validation_p = run_knn(i, train['train_inputs'],
                    train['train_targets'],
                    test['test_inputs'])
        count = 0
        for j in range(len(validation_p)):
            if validation_p[j][0] == test['test_targets'][j][0]:
                count += 1
        print('K=' + str(i) +', Classification rate=' +
              str(count * 100.0/len(validation_p)) +'%')  
        graph.append(count * 100.0/len(validation_p))
    plt.plot([1,3,5,7,9], graph)
    plt.show()