""" Methods for doing logistic regression."""

import numpy as np
from utils import sigmoid

def logistic_predict(weights, data):
    """
    Compute the probabilities predicted by the logistic classifier.

    Note: N is the number of examples and 
          M is the number of features per example.

    Inputs:
        weights:    (M+1) x 1 vector of weights, where the last element
                    corresponds to the bias (intercepts).
        data:       N x M data matrix where each row corresponds 
                    to one data point.
    Outputs:
        y:          :N x 1 vector of probabilities. This is the output of the classifier.
    """

    # TODO: Finish this function
    N, M = data.shape
    fill = np.ones((N, 1))
    data = np.append(data, fill, axis=-1)    
    y = np.transpose(sigmoid(np.dot(np.transpose(weights), np.transpose(data))))
    return y

def evaluate(targets, y):
    """
    Compute evaluation metrics.
    Inputs:
        targets : N x 1 vector of binary targets. Values should be either 0 or 1
        y       : N x 1 vector of probabilities.
    Outputs:
        ce           : (scalar) Cross entropy.  CE(p, q) = E_p[-log q].  Here
                       we want to compute CE(targets, y).
        frac_correct : (scalar) Fraction of inputs classified correctly.
    """
    # TODO: Finish this function
    N, M = targets.shape
    ce = 0
    count = 0
    for i in range(len(targets)):
        ce -= targets[i] * np.log(y[i]) + (1 - targets[i]) * np.log(1 - y[i])
        if y[i] >= 0.5 and targets[i] == 1:
            count += 1
        elif y[i] < 0.5 and targets[i] == 0:
            count += 1
    return ce, (count + 1.0 - 1.0)/ N

def logistic(weights, data, targets, hyperparameters):
    """
    Calculate negative log likelihood and its derivatives with respect to weights.
    Also return the predictions.

    Note: N is the number of examples and 
          M is the number of features per example.

    Inputs:
        weights:    (M+1) x 1 vector of weights, where the last element
                    corresponds to bias (intercepts).
        data:       N x M data matrix where each row corresponds 
                    to one data point.
        targets:    N x 1 vector of binary targets. Values should be either 0 or 1.
        hyperparameters: The hyperparameters dictionary.

    Outputs:
        f:       The sum of the loss over all data points. This is the objective that we want to minimize.
        df:      (M+1) x 1 vector of derivative of f w.r.t. weights.
        y:       N x 1 vector of probabilities.
    """

    # TODO: Finish this function
    N, M = data.shape
    fill = np.ones((N, 1))
    data = np.append(data, fill, axis=-1)
    y = np.transpose(sigmoid(np.dot(np.transpose(weights), np.transpose(data))))
    z = np.dot(np.transpose(weights), np.transpose(data))
    
    f = np.dot(np.transpose(1 - targets), np.transpose(z)) + np.sum(np.log(1 + np.exp(-z)))
    df = np.transpose(np.dot(np.transpose(1 - targets), data) - np.dot(np.exp(-z)/(1+np.exp(-z)), data))
    
    return f, df, y


def logistic_pen(weights, data, targets, hyperparameters):
    """
    Calculate negative log likelihood and its derivatives with respect to weights.
    Also return the predictions.

    Note: N is the number of examples and 
          M is the number of features per example.

    Inputs:
        weights:    (M+1) x 1 vector of weights, where the last element
                    corresponds to bias (intercepts).
        data:       N x M data matrix where each row corresponds 
                    to one data point.
        targets:    N x 1 vector of binary targets. Values should be either 0 or 1.
        hyperparameters: The hyperparameters dictionary.

    Outputs:
        f:             The sum of the loss over all data points. This is the objective that we want to minimize.
        df:            (M+1) x 1 vector of derivative of f w.r.t. weights.
    """

    # TODO: Finish this function
    N, M = data.shape
    fill = np.ones((N, 1))
    data = np.append(data, fill, axis=-1)
    pen = hyperparameters['weight_regularization']
    y = np.transpose(sigmoid(np.dot(np.transpose(weights), np.transpose(data))))
    z = np.dot(np.transpose(weights), np.transpose(data))
    
    fp = np.dot(np.transpose(1 - targets), np.transpose(z)) + np.sum(np.log(1 + np.exp(-z)))
    f = fp + np.sum(weights * weights * pen / 2)
    
    df = np.transpose(np.dot(np.transpose(1 - targets), data) + np.transpose(pen * weights) - np.dot(np.exp(-z)/(1+np.exp(-z)), data))
    return f, df, y
